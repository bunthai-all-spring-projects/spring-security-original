Endpoints

- http://localhost:8080/v1/register as public
- http://localhost:8080/v1/all with Role: STAFF, ADMIN, SUPER
- http://localhost:8080/v1/data with Role: ADMIN, SUPER
- http://localhost:8080/v1/resource with Role: SUPER


User
- jack, pass: 123, role: STAFF
- jam, pass: 123, role: STAFF, ADMIN, SUPER

Available for both FromLogin and Basic Authentication

Example
curl --request GET \
--url http://localhost:8080/v1/all \
--header 'Authorization: Basic amFjazoxMjM=' \
--header 'Content-Type: application/json'