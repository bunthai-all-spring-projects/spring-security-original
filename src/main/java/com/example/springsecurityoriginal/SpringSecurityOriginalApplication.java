package com.example.springsecurityoriginal;

import com.example.springsecurityoriginal.domain.AppUser;
import com.example.springsecurityoriginal.domain.AppUserRole;
import com.example.springsecurityoriginal.domain.Role;
import com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.Category;
import com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.Customer;
import com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.Order;
import com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.test_repository.CustomerRepository;
import com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.test_repository.OrderRepository;
import com.example.springsecurityoriginal.repository.AppUserRepository;
import com.example.springsecurityoriginal.repository.AppUserRoleRepository;
import com.example.springsecurityoriginal.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@SpringBootApplication
@EnableJpaAuditing
public class SpringSecurityOriginalApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityOriginalApplication.class, args);
	}

	@Autowired
	AppUserRepository appUserRepository;

	@Autowired
	AppUserRoleRepository appUserRoleRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	ApplicationContext context;

	@Override
	public void run(String... args) throws Exception {

		var users = List.of(
				AppUser.builder().username("jack").password(passwordEncoder.encode("123")).build(),
				AppUser.builder().username("jam").password(passwordEncoder.encode("123")).build()
		);

		users = appUserRepository.saveAll(users);

		var roles = List.of(
				Role.builder().name("STAFF").build(),
				Role.builder().name("ADMIN").build(),
				Role.builder().name("SUPER").build()
		);
		roles = roleRepository.saveAll(roles);


		var userRoles = List.of(
				AppUserRole.builder().appUser(users.get(0)).role(roles.get(0)).build(),
				AppUserRole.builder().appUser(users.get(1)).role(roles.get(0)).build(),
				AppUserRole.builder().appUser(users.get(1)).role(roles.get(1)).build(),
				AppUserRole.builder().appUser(users.get(1)).role(roles.get(2)).build()
		);

		appUserRoleRepository.saveAll(userRoles);


		var categories = List.of(
				Category.builder().type("Classic").customer(Customer.builder().id(1).build()).build(),
				Category.builder().type("Premium").customer(Customer.builder().id(1).build()).build(),
				Category.builder().type("VIP").customer(Customer.builder().id(1).build()).build()
		);
		var categories2 = List.of(
				Category.builder().type("Classic").customer(Customer.builder().id(2).build()).build(),
				Category.builder().type("Premium").customer(Customer.builder().id(2).build()).build(),
				Category.builder().type("VIP").customer(Customer.builder().id(2).build()).build()
		);


		var customerRepository = context.getBean(CustomerRepository.class);
		var customers = List.of(
				Customer.builder().name("jiji").gender("M").categories(categories).build(),
				Customer.builder().name("tata").gender("F").categories(categories2).build()
		);
		customers = customerRepository.saveAll(customers);




		var orderRepository = context.getBean(OrderRepository.class);
		var orders = List.of(
				Order.builder().product("coca").customer(customers.get(0)).build(),
				Order.builder().product("fanta").customer(customers.get(0)).build()
		);
		orderRepository.saveAll(orders);

	}
}
