package com.example.springsecurityoriginal.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

    @Autowired
    UserDetailsService userDetailsService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {


        http.cors(cors -> cors.disable());
        http.csrf(csrf -> csrf.disable());

        http.authorizeHttpRequests(authorize -> {
            authorize

                    .requestMatchers("/register").permitAll()
                    .requestMatchers("/resource").hasAnyRole("SUPER")
                    .requestMatchers("/data").hasAnyRole("ADMIN", "SUPER")
                    .requestMatchers("/all").hasAnyRole("STAFF", "ADMIN", "SUPER")
                    .anyRequest().authenticated();
        });//.formLogin(Customizer.withDefaults()).httpBasic(Customizer.withDefaults());


        http.formLogin(form -> {
            try {
                form.defaultSuccessUrl("/all");
                form.init(http);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        http.httpBasic(basic -> {
           basic.init(http);
        });

        return http.build();
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}
