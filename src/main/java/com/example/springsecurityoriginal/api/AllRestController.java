package com.example.springsecurityoriginal.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/all")
public class AllRestController {

    @GetMapping
    public String all() {
        return "all";
    }

}
