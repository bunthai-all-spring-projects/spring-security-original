package com.example.springsecurityoriginal.api.pub;

import com.example.springsecurityoriginal.domain.AppUser;
import com.example.springsecurityoriginal.domain.AppUserRole;
import com.example.springsecurityoriginal.domain.Role;
import com.example.springsecurityoriginal.repository.AppUserRepository;
import com.example.springsecurityoriginal.repository.AppUserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("register")
public class RegistrationRestController {

    @Autowired
    AppUserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    AppUserRoleRepository userRoleRepository;

    @PostMapping
    public AppUser register(@RequestBody Map body) {
        var username = (String) body.getOrDefault("username", null);
        var password = (String) body.getOrDefault("password", null);
        var user = AppUser.builder()
                .username(username)
                .password(encoder.encode(password))
                .build();
        user = userRepository.save(user);

        var userRole = AppUserRole.builder().appUser(user).role(Role.builder().id(1).build()).build();
        userRoleRepository.save(userRole);

        return user;
    }

}
