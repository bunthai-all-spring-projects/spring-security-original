package com.example.springsecurityoriginal.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/resource")
public class ResourceRestController {

    @GetMapping
    public String resource() {
        return "resource";
    }

}
