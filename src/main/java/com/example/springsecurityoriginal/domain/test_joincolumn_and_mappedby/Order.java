package com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby;

import com.example.springsecurityoriginal.domain.nonpersist.Auditor;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "\"order\"")
public class Order extends Auditor {

    private String product;

    @ManyToOne
    private Customer customer;

}
