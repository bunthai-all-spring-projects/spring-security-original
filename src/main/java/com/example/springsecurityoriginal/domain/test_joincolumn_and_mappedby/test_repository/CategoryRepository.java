package com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.test_repository;

import com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
