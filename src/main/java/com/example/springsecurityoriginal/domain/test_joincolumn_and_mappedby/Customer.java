package com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby;

import com.example.springsecurityoriginal.domain.nonpersist.Auditor;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Customer extends Auditor {

    private String name;
    private String gender;

    @OneToMany(mappedBy = "customer", cascade = {CascadeType.ALL})
    private List<Category> categories;

}
