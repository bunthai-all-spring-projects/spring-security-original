package com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby;

import com.example.springsecurityoriginal.domain.nonpersist.Auditor;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Category extends Auditor {

    private String type;

    @ManyToOne
    private Customer customer;
}
