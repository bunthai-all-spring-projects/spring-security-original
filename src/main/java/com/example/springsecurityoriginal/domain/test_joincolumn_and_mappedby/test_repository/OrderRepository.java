package com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.test_repository;

import com.example.springsecurityoriginal.domain.test_joincolumn_and_mappedby.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
}
