package com.example.springsecurityoriginal.repository;

import com.example.springsecurityoriginal.domain.AppUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRoleRepository extends JpaRepository<AppUserRole, Long> {
}
